FROM maven:3.6.3-jdk-13
WORKDIR /src
COPY pom.xml pom.xml
RUN mvn dependency:go-offline
COPY . .
RUN mvn spring-javaformat:apply
RUN mvn package
EXPOSE 8080
CMD java -jar target/*.jar